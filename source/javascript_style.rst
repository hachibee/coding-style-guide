

==============================================
JavaScriptのコーディング規約
==============================================

はじめに
==============================================

一通り目を通しましょう

| `Mozilla <https://developer.mozilla.org/ja/docs/JavaScript_style_guide>`_
| `Google <http://cou929.nu/data/google_javascript_style_guide/>`_
| `Node.js <http://popkirby.github.io/contents/nodeguide/style.html>`_


設定について
==============================================

- エディタ
    see :ref:`editor-settings-label`

- エンコーディング
    *BOMなしの* UTF-8を使用します。文字コードを適切に扱えないエディタは使用してはいけません。

- 改行コード
    LFを使います。CRLFは使わないでください。

- タブ
    エディタの項にもあるように、スペース2つとします。

- 圧縮
    Closure Compilerを使います。
    圧縮レベルは[]とします。


コーディングスタイル
==============================================

- strictモード
    | 必ず使用します。
    | 既存の連携などが要求される部分については、即時関数を使ってスコープを制御します。


- 静的解析
    | `JSHint <https://github.com/jshint/jshint/>`_ を使います。
    | また、既存のコードはその限りではありませんが、コミットするコードは全てのエラーが解決されていなければなりません。


- 特に理由がなければ
    HaxeかTypeScriptを使いましょう。
