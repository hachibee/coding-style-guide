
==============================================
コーディングをサポートするツールについて
==============================================


必須のもの
==============================================

何故必須なのか
------------------

pep8の準拠度などのチェックを人間がするのは無駄だからです。


コマンドライン
------------------

前提
^^^^

easy_install, pipの使い方を事前に調べておいて、使えるようになっておいてください。

 - virtualenv

    pypi経由でインストールします。

    サンドボックスを作り、その中でPythonモジュールを管理してくれます。
    大量のモジュールをいれたせいで、依存性の管理が困難になるのを防ぎます。必須です。


 - flake8

    pypi経由でインストールします。

    pep8とコードの静的解析を行ってくれます。
    なお、pep8のうち無視するもの（E501とか）については要相談。


 - PyChecker

    pypi経由でインストールします。

    コードの静的解析を行ってくれます。
    こちらは少々ヘビィなので要相談です。


 - autopep8

    pypi経由でインストールします。

    自動でpep8準拠にコードを整形してくれます。要相談。


 - pytest

    pypi経由でインストールします。

    テストランナーです。


 - ctags

    タグファイルを生成します


.. _editor-settings-label:

エディタの設定
------------------

 - Vim

最低限必須の設定
^^^^^^^^^^^^^^^^^^

 - .vimrc

    個別の設定項目自体については`:help`を引いてください。

.. code-block:: vim

    set nocompatible
    filetype plugin indent on
    syntax enable
    set t_Co=256
    set fileformats=unix,dos
    set smarttab
    set expandtab
    set shiftround
    set shiftwidth=4 softtabstop=4
    set list
    set number
    set listchars=tab:>-,trail:~
    set ambiwidth=double
    if has('path_extra')
        set tags& tags+=.tags,tags
    endif
    set laststatus=2
    set showtabline=2


- $HOME/.vim/after/ftplugin/python.vim

.. code-block:: vim

    if exists('b:did_ftplugin_python')
        finish
    endif
    let b:did_ftplugin_python = 1

    setlocal foldmethod=indent
    setlocal commentstring=#%s
    let g:python_highlight_all = 1


- $HOME/.vim/after/ftplugin/javascript.vim

.. code-block:: vim

    if exists('b:did_ftplugin_javascript')
        finish
    endif
    let b:did_ftplugin_javascript = 1

    setlocal tabstop=2
    setlocal softtabstop=2
    setlocal shiftwidth=2



必須プラグイン
^^^^^^^^^^^^^^^^

| 導入の前に、Flake8-vimに関してはVimのPython拡張が有効になっている必要があります。
| 貴方が使っているOSがDebianであれば、'sudo apt-get install vim-nox'とすることでVimのPython拡張が有効になります。
| 他のOSを使っている方は各自調べてください。

 - `vim-scripts/Flake8-vim <https://github.com/vim-scripts/Flake8-vim>`_

    内部でflake8を使い、pep8とコードの静的解析を行い、エディタ上にエラー箇所を表示してくれます。

    [注意] ただし、使っているPythonが2.6の場合にエラーが発生するのでエラーが発生する可能性があります。
    発生したら矢野か小倉にきいてください。


 - `cohama/vim-hier <https://github.com/cohama/vim-hier>`_

    flake8などがQuickfixに出力した結果を使い、画面上にハイライト表示してくれます。

 - `dannyob/quickfixstatus <https://github.com/dannyob/quickfixstatus>`_

    Quickfixの出力を使い、カーソル位置にエラーがあったら情報をステータスラインに表示してくれます。


以下は、上記プラグインの設定例になります。

.. code-block:: vim

    " Flake8-vim {{{
    let g:PyFlakeOnWrite = 1
    " 無視する警告の種類
    " E501 => 行ごとの文字数制限, E121 => 次行のインデントはひとつだけ, E303 => 改行の数が多すぎる
    let g:PyFlakeDisabledMessages = 'E501,E121,E303'
    " エラー行のマーカー。hierあればいらないので非表示に
    let g:PyFlakeSigns = 0
    " flake8-autoをかけるためのコマンド。visual-modeでの範囲選択に対応
    let g:PyFlakeRangeCommand = 'Q'
    " }}}

    " キーバインドの設定は好みで。以下は例
    nnoremap [Show] <Nop>
    nmap <Space>s [Show]
    nnoremap [Show]hc  :<C-u>HierClear<CR>
    nnoremap [Show]hs  :<C-u>HierStart<CR>
    nnoremap [Show]hp  :<C-u>HierStop<CR>
    nnoremap [Show]hu  :<C-u>HierUpdate<CR>

    " preview error line in quickfix
    nnoremap <M-p> :<C-u>cp<CR>
    " next error line in quickfix
    nnoremap <M-n> :<C-u>cn<CR>



推奨されるもの
==============================================

必須ではないが、特に理由がない限りは導入を推奨するものたち


Vim
------------------


 - プラグイン管理用プラグイン

     手動でruntimepathを管理するのはハマる原因になるのでやめましょう。

      - `Shougo/Neobundle (おすすめ) <https://github.com/Shougo/neobundle.vim>`_

      - Vundle

      - pathogen


 - `tell-k/vim-autopep8 <https://github.com/tell-k/vim-autopep8>`_

    編集中のファイルにautopep8をかけてくれます。


 - `kana/vim-smartchr <https://github.com/kana/vim-smartchr>`_ と `kana/vim-smartinput <https://github.com/kana/vim-smartinput>`_

    演算子の両脇にスペースを挿入する、改行時は末尾のスペースを削除する、などの動作を定義することが出来るプラグインです。
    適切に定義しておけば、pep8に怒られることも少なくなる化と思います。


 - `thinca/vim-quickrun <https://github.com/thinca/vim-quickrun>`_

    編集中のバッファを実行してくれます。doctestを確認しながら開発することが出来ます。

