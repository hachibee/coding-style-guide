.. coding style guide documentation master file, created by
   sphinx-quickstart on Tue Sep 24 16:10:44 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to coding style guide's documentation!
==============================================

.. toctree::
   :maxdepth: 2

   about_contents


内容
-----

.. toctree::
   :maxdepth: 2

   tools

   python_style

   javascript_style


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

